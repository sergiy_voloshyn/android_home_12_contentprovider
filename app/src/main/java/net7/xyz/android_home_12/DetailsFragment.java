package net7.xyz.android_home_12;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import net7.xyz.android_home_12.dbaseprovider.NotesTable;
import net7.xyz.android_home_12.model.ItemData;


public class DetailsFragment extends Fragment {

    @BindView(R.id.text_caption)
    EditText textCaption;

    @BindView(R.id.text_data)
    EditText textData;
    String itemId = "";


    private OnFragmentInteractionDetailsListener mListener;

    public static DetailsFragment newInstance() {
        return new DetailsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, v);

        ItemData itemData = mListener.getData();
        if (itemData != null) {
            itemId = itemData.getIdItem();
            textCaption.setText(itemData.getCaptionItem());
            textData.setText(itemData.getTextItem());
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        ItemData itemData = mListener.getData();
        if (itemData != null) {
            itemId = itemData.getIdItem();
            textCaption.setText(itemData.getCaptionItem());
            textData.setText(itemData.getTextItem());
        }
    }

    @OnClick(R.id.save_button)
    public void onSaveButtonPressed() {


        ContentValues cv = new ContentValues();
        cv.put(NotesTable.Columns.TITLE, textCaption.getText().toString());
        cv.put(NotesTable.Columns.TEXT, textData.getText().toString());
        getContext().getContentResolver().update(Uri.withAppendedPath(NotesTable.CONTENT_URI, itemId), cv, null, null);

        mListener.onDetailsInteraction();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionDetailsListener) {
            mListener = (OnFragmentInteractionDetailsListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionDetailsListener {

        void onDetailsInteraction();

        ItemData getData();
    }
}
