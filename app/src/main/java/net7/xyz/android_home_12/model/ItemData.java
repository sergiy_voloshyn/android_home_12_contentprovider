package net7.xyz.android_home_12.model;

/**
 * Created by user on 18.02.2018.
 */

public class ItemData {
    String idItem;
    String captionItem;
    String textItem;

    public ItemData(String idItem, String captionItem, String textItem) {
        this.idItem = idItem;
        this.captionItem = captionItem;
        this.textItem = textItem;
    }

    public String getIdItem() {
        return idItem;
    }

    public String getCaptionItem() {
        return captionItem;
    }

    public String getTextItem() {
        return textItem;
    }


}

