package net7.xyz.android_home_12;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by user on 16.02.2018.
 */

abstract class MyTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }


}
