package net7.xyz.android_home_12;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import net7.xyz.android_home_12.model.ItemData;
/*

1. Взяв за основу проект, добавить в него возможности:
добавлять заметку с заголовким и текстом на экране отдельно от списка;
изменять существующую заметку на экране отдельно от списка;
просматривать существующую заметку на экране отдельно от списка;
удалять заметку на экране списка (у каждого элемента должна быть иконка удаления);
расширить элементы списка, добавив туда заголовок и текст. Текст при отображении в элементе списка
не должен быть больше, чем в одну строку.

1.1* Добавить на экран со списком строку поиска заметки по ключевым словам. Поиск должен осуществляться как по заголовку, так и по тексту.
При каждом изменении строки поиска, список должен фильтроваться согласно введенным данным.

 */

public class MainActivity extends AppCompatActivity implements ListFragment.OnListFragmentInteractionListener, DetailsFragment.OnFragmentInteractionDetailsListener {
    ItemData itemDataMain;
    //  Context contextData;

    ListFragment listFragment;
    DetailsFragment detailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        detailsFragment = DetailsFragment.newInstance();
        listFragment = ListFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main, listFragment)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onDetailsInteraction() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main, listFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public ItemData getData() {
        return itemDataMain;
    }

    @Override
    public void onListInteraction(ItemData itemData) {

        itemDataMain = itemData;

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main, detailsFragment)
                .addToBackStack(null)
                .commit();
    }
}