package net7.xyz.android_home_12;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import net7.xyz.android_home_12.dbaseprovider.NotesTable;
import net7.xyz.android_home_12.model.ItemData;

public class ListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    MyAdapter adapter;
    @BindView(R.id.list)
    ListView list;

    @BindView(R.id.find_edit)
    EditText findText;

  //  String idItem = "";
    //Cursor cursorFind;

    public static final String STRINGTOFIND = "STRINGTOFIND";

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    private OnListFragmentInteractionListener mListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, v);

        adapter = new MyAdapter(getContext());
        list.setAdapter(adapter);
        getLoaderManager().initLoader(0, null, this);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    Cursor cursor = (Cursor) list.getItemAtPosition(position);

              //      idItem = cursor.getString(0);

                    mListener.onListInteraction(new ItemData(cursor.getString(0),
                            cursor.getString(1), cursor.getString(2)));
                }
            }
        });


        findText.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

                if (!s.toString().equals("")) {
                    //restart with new parameters
                    Bundle args = new Bundle();
                    args.putString(STRINGTOFIND, s.toString());
                    getLoaderManager().restartLoader(0, args, ListFragment.this);
                } else {
                    getLoaderManager().restartLoader(0, null, ListFragment.this);
                }
            }
        });
        return v;
    }

    @OnClick(R.id.add_floating)
    public void onClickFloatingButton() {

        ContentValues cv = new ContentValues();
        cv.put(NotesTable.Columns.TEXT, "text");
        cv.put(NotesTable.Columns.TITLE, "title");
        getContext().getContentResolver().insert(NotesTable.CONTENT_URI, cv);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnListFragmentInteractionListener {

        //void onListInteraction(Cursor cursor);
        void onListInteraction(ItemData cursor);

    }

    public class MyAdapter extends CursorAdapter {

        public MyAdapter(Context context) {
            super(context, null, false);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.fragment_list_item, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            ButterKnife.bind(viewHolder, v);

            v.setTag(viewHolder);
            return v;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            ViewHolder viewHolder = (ViewHolder) view.getTag();
            final String id = cursor.getString(0);
            viewHolder.imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //   Toast.makeText(getContext(), id, Toast.LENGTH_SHORT).show();
                    getContext().getContentResolver().delete(Uri.withAppendedPath(NotesTable.CONTENT_URI, id), null, null);
                }
            });

            if (cursor != null) {
                viewHolder.numberItem.setText(cursor.getString(0));
                viewHolder.captionItem.setText(cursor.getString(1));
                viewHolder.textItem.setText(cursor.getString(2));
            }

        }
    }


    static class ViewHolder {
        @BindView(R.id.number_item)
        TextView numberItem;

        @BindView(R.id.caption_item)
        TextView captionItem;

        @BindView(R.id.text_item)
        TextView textItem;

        @BindView(R.id.image_delete)
        ImageView imageDelete;
    }

               /*
                 Обрабатывает запрос на выборку данных
     * @param uri - указатель на таблицу
     * @param projection - список столбцов для выборки
     * @param selection - условия выборки
     * @param selectionArgs - аргументы для условия выборки
     * @param sortOrder - условия сортировки
     * @return - курсор с результатами выборки
                 */

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (args != null) {
            CursorLoader cursorLoader = new CursorLoader(getContext(), NotesTable.CONTENT_URI,
                    new String[]{NotesTable.Columns._ID, NotesTable.Columns.TITLE, NotesTable.Columns.TEXT},
                    NotesTable.Columns.TEXT + " LIKE ?" + " OR " + NotesTable.Columns.TITLE + " LIKE ?",
                    new String[]{"%" + args.getString(STRINGTOFIND) + "%", "%" + args.getString(STRINGTOFIND) + "%"},
                    null);
            return cursorLoader;
        } else {
            return new CursorLoader(getContext(), NotesTable.CONTENT_URI, null, null, null, null);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

}
