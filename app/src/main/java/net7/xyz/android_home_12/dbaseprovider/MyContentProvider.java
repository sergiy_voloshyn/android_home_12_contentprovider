package net7.xyz.android_home_12.dbaseprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import net7.xyz.android_home_12.BuildConfig;


public class MyContentProvider extends ContentProvider {

    //поля необходимые для идентификации таблиц
    private static final String AUTHORITY = BuildConfig.AUTHORITY;
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    //список таблиц
    private final NotesTable messagesTable = new NotesTable();
    //...

    //инициализвания URI_MATCHER с таблицами для упрощения идендификации
    static {
        URI_MATCHER.addURI(AUTHORITY, NotesTable.NAME, MappedUri.NOTES);
        URI_MATCHER.addURI(AUTHORITY, NotesTable.NAME + "/#", MappedUri.NOTES_ITEM);
    }

    //собственно база данных
    private SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        //получение доступа в базу в режиме writable
        database = new DataBaseHelper(getContext()).getWritableDatabase();
        return true;
    }

    /**
     * Обрабатывает запрос на выборку данных
     * @param uri - указатель на таблицу
     * @param projection - список столбцов для выборки
     * @param selection - условия выборки
     * @param selectionArgs - аргументы для условия выборки
     * @param sortOrder - условия сортировки
     * @return - курсор с результатами выборки
     */
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //получаем таблицу по указателю
        Table table = getTable(uri);
        //создаем конструктов запросов к таблице
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(table.getName());
        //если указатель содержить id записи (указывает не на всю таблицу, а на конкретный элемент)
        if (isItemUri(uri)) {
            //добавляем id к условию выборки
            builder.appendWhere(table.getColumnId() + "=" + uri.getLastPathSegment());
        }
        //запрос к базе со всеми параметрами
        Cursor cursor = builder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        //подписываемся на изменения текущей таблицы
        setNotificationUri(cursor, uri);
        return cursor;
    }

    /**
     * Обрабатывает запрос на вставку данных в таблицу
     * @param uri - указатель на таблику
     * @param values - значения для вставки в таблицу
     * @return указатель на вставленный элемент
     */
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        //получаем таблицу по указателю
        Table table = getTable(uri);
        //вставка данных в таблицу
        long rowId = database.insert(table.getName(), null, values);
        //получение указателя по id вставленной записи
        Uri resultUri = table.getItemUri(rowId);
        //оповещаем про изменения в таблице
        notifyChange(uri);
        return resultUri;
    }

    /**
     * Удаляет записи по условию (если условие не задано - очищает всю таблицу)
     * @param uri - указатель на таблицу
     * @param selection - условия выборки
     * @param selectionArgs - аргументы для условия выборки
     * @return количество удаленных записей
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        //получаем таблицу по указателю
        Table table = getTable(uri);
        //если указатель содержить id записи и условия выборки отсутствуют
        if (isItemUri(uri) && TextUtils.isEmpty(selection)) {
            //добавляем id к условию выборки
            selection = table.getColumnId() + "=" + uri.getLastPathSegment();
        }
        //удаление данных из таблицы
        int deletedRowNum = database.delete(table.getName(), selection, selectionArgs);
        //оповещение про изменения в таблице
        notifyChange(uri);
        return deletedRowNum;
    }


    /**
     *
     * @param uri - указатель на таблицу
     * @param values - новые значения для обновления даных в таблице
     * @param selection - условия выборки записей для обновления
     * @param selectionArgs - аргументы для условия выборки
     * @return - количество обновленных записей
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        //получаем таблицу по указателю
        Table table = getTable(uri);
        //если указатель содержить id записи и условия выборки отсутствуют
        if (isItemUri(uri) && TextUtils.isEmpty(selection)) {
            //добавляем id к условию выборки
            selection = table.getColumnId() + "=" + uri.getLastPathSegment();
        }
        //обновляем данные в таблице
        int updatedRowsNum = database.update(table.getName(), values, selection, selectionArgs);
        //если ничего не обновилось
        if (updatedRowsNum == 0) {
            //вставляем записть в таблицу, как новую
            database.insert(table.getName(), null, values);
            updatedRowsNum = 1;
        }
        //уведомляем про изменнеия
        notifyChange(uri);
        return updatedRowsNum;
    }

    /**
     * Возвращает тип таблицы или бросает исключение, если она не наедена
     */
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case MappedUri.NOTES:
                return messagesTable.getContentType();
            default:
                throw new IllegalArgumentException("Uri: " + uri + " is not supported");
        }
    }

    /**
     * Ищем таблиуцу по указателю
     * @param uri - указатель
     * @return - таблица
     */
    private Table getTable(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case MappedUri.NOTES:
            case MappedUri.NOTES_ITEM:
                return messagesTable;

            default:
                throw new IllegalArgumentException("Uri: " + uri + " is not supported");
        }
    }

    /**
     * Проверяет, указывает ли uri на конкретный элемент в таблице
     */
    private boolean isItemUri(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case MappedUri.NOTES_ITEM:
                return true;
        }
        return false;
    }

    /**
     * Подписывается на изменения в таблице
     */
    private void setNotificationUri(Cursor cursor, Uri uri) {
        Context context = getContext();
        if (context != null) {
            cursor.setNotificationUri(context.getContentResolver(), uri);
        }
    }

    /**
     * Уведомляет об изменениях в таблице
     */
    private void notifyChange(Uri uri) {
        Context context = getContext();
        if (context != null) {
            context.getContentResolver().notifyChange(uri, null);
        }
    }

    /**
     * Список таблиц и указателей
     */
    public interface MappedUri {
        int NOTES = 0;
        int NOTES_ITEM = 1;
    }
}
