package net7.xyz.android_home_12.dbaseprovider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

import net7.xyz.android_home_12.BuildConfig;

public class NotesTable implements Table {

    public static final String NAME = "notes";

    public static final String CREATE = "CREATE TABLE "
            + NAME
            + "("
            + Columns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Columns.TITLE + " TEXT, "
            + Columns.TEXT + " TEXT "
            + ");";

    public static final String DROP = "DROP TABLE IF EXISTS " + NAME;

    private static final String MIME_TYPE = "vnd." + BuildConfig.AUTHORITY + "_" + NAME;

    private static final String CONTENT_PATH = "content://" + BuildConfig.AUTHORITY + "/" + NAME;

    public static final Uri CONTENT_URI = Uri.parse(CONTENT_PATH);

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getColumnId() {
        return Columns._ID;
    }

    public String getContentItemType() {
        return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + MIME_TYPE;
    }

    public String getContentType() {
        return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + MIME_TYPE;
    }

    public Uri getItemUri(long id) {
        return Uri.parse(CONTENT_PATH + "/" + id);
    }

    public interface Columns extends BaseColumns {
        String TITLE = "title";
        String TEXT = "text";
    }

    public interface ColumnIndices {
        int _ID = 0;
        int TITLE = 1;
        int TEXT = 2;
    }

    public interface Selection {
        String USER_ID = Columns.TITLE + " = ?";
    }

}
