package net7.xyz.android_home_12.dbaseprovider;

import android.net.Uri;

public interface Table {

    String getName();

    String getColumnId();

    Uri getItemUri(long id);

}